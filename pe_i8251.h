/*
 * pe_i8251.h
 *
 *  Created on: Mar 3, 2018
 *      Author: santiago
 */

#ifndef PE_I8251_H_
#define PE_I8251_H_

#include "pe_base.h"

typedef struct
{
	pe_base_peri_t		base;

} pe_i8251_t;

void pe_i8251_init(pe_i8251_t * new_pe);

#endif /* PE_I8251_H_ */
