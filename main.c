/*
 * main.c
 *
 *  Created on: Feb 28, 2018
 *      Author: santiago
 */


#include <stdio.h>

#include "console_impl.h"

#include "8080emu.h"

#include "pe_base.h"
#include "pe_sram.h"
#include "pe_i8155.h"
#include "pe_bq3285.h"
#include "pe_i8251.h"
#include "pe_rom.h"
#include "pe_interaction.h"

#include <stdint.h>
#include <stdbool.h>

#include <unistd.h>



pe_rom_t rom1;
pe_rom_t rom2;
pe_rom_t rom3;
pe_rom_t rom4;
pe_i8155_t i8155_f800;
pe_i8155_t i8155_b800;
pe_sram_64k_t sram_main;
pe_bq3285_t rtc;
pe_i8251_t uart;


State8080	cpu;
uint8_t		rmk_mode = 0xe0;

volatile bool close_app = false;


uint8_t cpu_in(uint16_t port)
{
	return pe_io_read(port);
}

void cpu_out(uint16_t port, uint8_t data)
{
	switch (port)
	{
		case 0xE0:
		case 0xE1:
		//case 0x77: // ???
		{
			/* ROM present in 0x0000 thru 0x0FFF */
			/* Backup user ram */
			//memcpy(shadow_ram, state->memory, 0x1000);
			//memcpy(shadow_ram, sram_main._sram, 0x1000);
			rom1.base.enabled = true;
			/* Put the rom again there */
			//ReadFileIntoMemoryAt("gmon.bin", 0);

			rmk_mode = port;

			//printf("! ROM selected, PC = 0x%.4x, mode = %.2x\n", cpu.pc, port);
			break;
		}
		case 0xE2:
		case 0xE3:
		//case 0x7E: // ???
		{
			/* RAM present in 0x0000 thru 0x0FFF */
			/* Reload user ram */
			//memcpy(state->memory, shadow_ram, 0x1000);
			//memcpy(sram_main._sram, shadow_ram, 0x1000);
			rom1.base.enabled = false;

			rmk_mode = port;

			//printf("! RAM selected, PC = 0x%.4x, mode = %.2x\n", cpu.pc, port);
			break;
		}
		default:
		{
			pe_io_write(port, data);
		}
	}
}






void cpu_mem_write(uint16_t address, uint8_t value)
{
	if (rmk_mode == 0xE2 || rmk_mode == 0xE3)
	{
		//printf("* RAM Write in %.2x mode: %.2x <- %.4x in PC = %.4x\n", rmk_mode, value, address, state->pc);
	}
	pe_mem_write(address, value);
}

uint8_t cpu_mem_read(uint16_t address)
{
	return pe_mem_read(address);
}









int rmk_run(State8080 * state)
{
	unsigned char	op;
	int				cycles;

	//op = &state->memory[state->pc];
	/*sram_main.base.mread(state->pc, &op, &PE_AS_BASE(sram_main));

	if (state->pc == 0x014b)
	{
		//cycle_count = cycle_count;
	}
	if (state->pc == 0x0)
	{
		//cycle_count = cycle_count;
	}
	if (op == 0xdb) //machine specific handling for IN
	{
		//sram_main.base.mread(state->pc + 1, &op, &PE_AS_BASE(sram_main));
		//state->a = cpu_in(state, op);
	}
	else if (op == 0xd3) //machine specific handling for OUT
	{
		//sram_main.base.mread(state->pc + 1, &op, &PE_AS_BASE(sram_main));
		//cpu_out(state, op, state->a);
	}*/

	op = state->mem_read(state->pc);

	/* Debug thing of the RMK board */
	if ((rmk_mode == 0xE2 && ((op & 0xC0) == 0x40) && ((op >> 3) & 7) == (op & 7)) ||
		(rmk_mode == 0xE3))
	{
		if (rmk_mode == 0xE3)
		{
			printf("\n# DBG: detected mode 0xE3 (opcode %.2x), SS (sleeping for 2), TRAP!\n", op);
			sleep(2);
		}
		else
		{
			printf("\n# DBG: detected debug opcode %.2x at 0x%.4x, TRAP!\n", op, state->pc);
		}

		GenerateTrap(state);
		cpu_out(0xE0, 0);
	}

	cycles = Emulate8080Op(state);


	return cycles;
}

pe_base1_t * pe_list[] =
{
	&PE_AS_BASE(uart),
	&PE_AS_BASE(i8155_f800),
	&PE_AS_BASE(i8155_b800),
	&PE_AS_BASE(rtc),
	&PE_AS_BASE(rom1),
	&PE_AS_BASE(rom2),
	&PE_AS_BASE(rom3),
	&PE_AS_BASE(rom4),
	&PE_AS_BASE(sram_main),
};

void pe_start(void)
{
	FILE *fh;

	/* Monitor ROM */
	fh = fopen("monitor.bin", "rb");
	//fh = fopen("gmon.bin", "rb");
	rom1.base.mem_start = 0x0000;
	rom1.base.mem_end = (4*1024) - 1 + rom1.base.mem_start;
	rom1.base.enabled = fh ? true : false;
	pe_rom_init(&rom1, fh, 0);

	/* Monitor Cont */
	fh = fopen("monitor_cont.bin", "rb");
	rom4.base.mem_start = 0xE000;
	rom4.base.mem_end = (4*1024) - 1 + rom4.base.mem_start;
	rom4.base.enabled = fh ? true : false;
	pe_rom_init(&rom4, fh, 0);

	/* Editor 0 */
	fh = fopen("editor0.bin", "rb");
	rom2.base.mem_start = 0x8000;
	rom2.base.mem_end = (4*1024) - 1 + rom2.base.mem_start;
	rom2.base.enabled = fh ? true : false;
	pe_rom_init(&rom2, fh, 0);

	/* Editor 1 */
	fh = fopen("editor1.bin", "rb");
	rom3.base.mem_start = 0x9000;
	rom3.base.mem_end = (4*1024) - 1 + rom3.base.mem_start;
	rom3.base.enabled = fh ? true : false;
	pe_rom_init(&rom3, fh, 0);

	/* Main SRAM */
	sram_main.base.mem_start = 0;
	sram_main.base.mem_end = (64*1024) - 1;
	sram_main.base.enabled = true;
	pe_sram_init(&PE_SRAM_AS_BASE(sram_main), 64*1024);

	/* i8155 @ level 1 */
	i8155_f800.base.mem_start = 0xF800;
	i8155_f800.base.mem_end = (64*1024) - 1;
	i8155_f800.base.io_start = 0xF8;
	i8155_f800.base.io_end = 0xFF;
	i8155_f800.base.enabled = true;
	pe_i8155_init(&i8155_f800);

	/* i8155 @ level 2 (RI/0-1) */
	i8155_b800.base.mem_start = 0xB800;
	i8155_b800.base.mem_end = 0xC000 - 1;
	i8155_b800.base.io_start = 0xB8;
	i8155_b800.base.io_end = 0xBF;
	i8155_b800.base.enabled = true;
	pe_i8155_init(&i8155_b800);

	/* RTC (taking the place of the RI/0-0) @ level 2 */
	rtc.base.mem_start = 0xB000;
	rtc.base.mem_end = 0xB7FF;
	rtc.base.enabled = true;
	pe_bq3285_init(&rtc);

	/* UART i8251 (Terminal) @ level 2 */
	uart.base.mem_start = 0xA800;
	uart.base.mem_end = 0xAFFF;
	uart.base.enabled = true;
	pe_i8251_init(&uart);


	pe_set_devices_array(pe_list, sizeof(pe_list)/(sizeof(pe_list[0])));
}

void pe_end(void)
{
	fclose(rom1.fhandle);
	fclose(rom2.fhandle);
	fclose(rom3.fhandle);
	fclose(rom4.fhandle);
}
/*
 *
s0
31
e0
f8
cd
28
f8
06
51
b8
c2
0d
00
c9
cd
30
f8
c3
03
00
x
xp0

 *
 */
int main(int argc, char *argv[])
{
	uint32_t		cycle_count = 0;
	uint32_t		cycles_executed = 0;

	console_start();
	pe_start();

	cpu.pc = 0;
	cpu.io_read = cpu_in;
	cpu.io_write = cpu_out;
	cpu.mem_read = cpu_mem_read;
	cpu.mem_write = cpu_mem_write;


	while (close_app == false)
	{
		if ((cycles_executed = rmk_run(&cpu)) == 0)
		{
			close_app = true;
		}

		usleep(5);

		cycle_count += cycles_executed;
	}


	/* Clean up */
	pe_end();
    console_end();

	return 0;
}
